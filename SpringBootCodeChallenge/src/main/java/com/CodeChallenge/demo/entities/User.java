package com.CodeChallenge.demo.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class User {
	
	@Id
	@GeneratedValue
	@Column(name="USER_ID", nullable = false)
	private Long id;
	@Column(name="NAME", nullable = false)
	private String name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name ="ROLE_ID")
	private Role roleId;
	@Column(name="CREATION_DATE", nullable = false)
	private Date creationDate;

	public User() {
	}
	
	public User(Long id, String name, Role roleId, Date creationDate) {
		super();
		this.id = id;
		this.name = name;
		this.roleId = roleId;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role getRoleId() {
		return roleId;
	}

	public void setRoleId(Role roleId) {
		this.roleId = roleId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
