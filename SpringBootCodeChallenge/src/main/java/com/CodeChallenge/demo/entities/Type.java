package com.CodeChallenge.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TYPE")
public class Type {
	
	@Id
	@GeneratedValue
	@Column(name="TYPE_ID", nullable = false)
	private Long typeId;
	@Column(name="DESCRIPTION", nullable = false)
	private String description;

	public Type() {
	}

	public Type(Long typeId, String description) {
		super();
		this.typeId = typeId;
		this.description = description;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
