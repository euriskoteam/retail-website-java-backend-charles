package com.CodeChallenge.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ITEM")
public class Item {
	
	@Id
	@GeneratedValue
	@Column(name="ITEM_ID", nullable = false)
	private Long itemId;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name ="TYPE_ID")
	private Type type;
	@Column(name="PRICE", nullable = false)
	private double price;
	
	public Item() {
	}

	public Item(Long itemId, Type type, double price) {
		super();
		this.itemId = itemId;
		this.type = type;
		this.price = price;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
