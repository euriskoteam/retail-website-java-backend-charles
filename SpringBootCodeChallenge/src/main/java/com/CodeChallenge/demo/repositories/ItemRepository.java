package com.CodeChallenge.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.CodeChallenge.demo.entities.Item;

public interface ItemRepository extends JpaRepository<Item, Long>{

}
