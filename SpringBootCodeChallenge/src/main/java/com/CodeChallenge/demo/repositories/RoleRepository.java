package com.CodeChallenge.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CodeChallenge.demo.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
