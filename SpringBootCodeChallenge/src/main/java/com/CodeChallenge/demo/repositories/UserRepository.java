package com.CodeChallenge.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CodeChallenge.demo.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

}