package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class SpringBootCodeChallengeApplicationTests extends AbstractTest {

	@Test
	public void EmployeTypeTest() {
	    long userId = 1;
	    String uri = "/api/getBill";
	    setUp();
		try {
		    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
		 	       .contentType(MediaType.APPLICATION_JSON_VALUE)
		 	       .param("item", "1,2,3")
		 	       .param("user", String.valueOf(userId))).andReturn();
		    String responseBody = mvcResult.getResponse().getContentAsString();
		    assertNotNull(responseBody);
		    assertEquals("540.0", responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}        
	}
	
	@Test
	public void AffiliateTypeTest() {
	    long userId = 5;
	    String uri = "/api/getBill";
	    setUp();
		try {
		    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
		 	       .contentType(MediaType.APPLICATION_JSON_VALUE)
		 	       .param("item", "1,2,3")
		 	       .param("user", String.valueOf(userId))).andReturn();
		    String responseBody = mvcResult.getResponse().getContentAsString();
		    assertNotNull(responseBody);
		    assertEquals("580.0", responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}        
	}
	
	@Test
	public void CustomerMoreThanTwoYearTypeTest() {
	    long userId = 4;
	    String uri = "/api/getBill";
	    setUp();
		try {
		    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
		 	       .contentType(MediaType.APPLICATION_JSON_VALUE)
		 	       .param("item", "1,2,3")
		 	       .param("user", String.valueOf(userId))).andReturn();
		    String responseBody = mvcResult.getResponse().getContentAsString();
		    assertNotNull(responseBody);
		    assertEquals("590.0", responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}        
	}
	
	@Test
	public void OtherTypeTest() {
	    long userId = 3;
	    String uri = "/api/getBill";
	    setUp();
		try {
		    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
		 	       .contentType(MediaType.APPLICATION_JSON_VALUE)
		 	       .param("item", "1,2,3")
		 	       .param("user", String.valueOf(userId))).andReturn();
		    String responseBody = mvcResult.getResponse().getContentAsString();
		    assertNotNull(responseBody);
		    assertEquals("590.0", responseBody);
		} catch (Exception e) {
			e.printStackTrace();
		}        
	}
}
