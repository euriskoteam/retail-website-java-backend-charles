Readme

Prerequisite 
1. Java 8
2. Spring Eclipse
3. Sql Server 2012

Steps to run the project

1. Import the project.
2. Run as spring boot.
3. After running either of above script wait for services to start.
4. You can access the webservice url on http://localhost:8080/api/getBill?item=1,2,3&user=3

Database:
You can find the sql database dump RETAIL_DB in the root folder.
1.Username: cha
2.Password chacha

Maven installation steps:
1.Install maven
2.Map to the root folder of the project where pom.xml exist.
3.Open command line and write the below:
	-mvn install
	-mvn test