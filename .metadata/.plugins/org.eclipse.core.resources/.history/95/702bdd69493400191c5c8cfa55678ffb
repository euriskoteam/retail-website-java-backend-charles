package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Item;
import com.example.demo.entities.User;

@Service
public class UserServiceImpl  {

	@PersistenceContext 
    private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<User> getUsers(){
		List<User> searchResults = new ArrayList<User>() ;
        searchResults = entityManager.createQuery("SELECT u FROM User u")
        		.getResultList();
        
        return searchResults;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean isEmployee(User user){
		List<User> searchResults = new ArrayList<User>() ;
        searchResults = entityManager.createQuery("SELECT u FROM User u where u.name = :name AND u.roleId.id = 1")
        		.setParameter("name", user.getName())
        		.getResultList();
        
        return searchResults.size() > 0;
	}

	@SuppressWarnings("unchecked")
	public Boolean isAffiliate(User user){
		List<User> searchResults = new ArrayList<User>() ;
        searchResults = entityManager.createQuery("SELECT u FROM User u where u.name = :name AND u.roleId.id = 3")
        		.setParameter("name", user.getName())
        		.getResultList();
        
        return searchResults.size() > 0;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean isGroceries(User user){
		List<User> searchResults = new ArrayList<User>() ;
        searchResults = entityManager.createQuery("SELECT u FROM User u where u.name = :name AND u.itemId.id = 1")
        		.setParameter("name", user.getName())
        		.getResultList();
        
        return searchResults.size() > 0;
	}
	
	@SuppressWarnings("unchecked")
	public List<Item> getItems(){
		List<Item> searchResults = new ArrayList<Item>() ;
        searchResults = entityManager.createQuery("SELECT u FROM Item u")
        		.getResultList();
        
        return searchResults;
	}
	
	@SuppressWarnings("unchecked")
	public List<Item> getItemsBtIds(List<Long> itemsId){
		List<Item> searchResults = new ArrayList<Item>() ;
        searchResults = entityManager.createQuery("SELECT u FROM Item u where u.itemId in (:itemsId)")
        		.setParameter("itemsId", itemsId)
        		.getResultList();
        
        return searchResults;
	}
}
